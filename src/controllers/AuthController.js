const Auth = require('../config/auth');
const User = require('../models/User');

const register = async(req,res) => {
    const generateHash = Auth.generateHash(req.body.password);
    const salt = generateHash.salt;
    const hash = generateHash.hash;

    const newUserData = {
        email: req.body.email,
        name: req.body.name,
        date_of_birth: req.body.date_of_birth,
        salt: salt,
        hash: hash,
    }

    try {
        const user = await User.create(newUserData);
        return res.status(201).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

const login = async (req, res) => {
    try {
        const user = await User.findOne({ where: { email: req.body.email } });

        const isValid = Auth.verifyPassword(req.body.password, user.dataValues.salt, user.dataValues.hash);
        if (isValid) {
            const token = Auth.generateJsonWebToken(user);
            res.status(200).json({
                message: "Usuário logado com sucesso.",
                token: token
            });
        }
        else {
            res.status(401).json({ message: "Voce entrou com a senha incorreta." });
        }
    } catch (err) {
        res.status(401).json({ message: "Esse usuário não existe." });
    }
};


const getDetails = (req, res) => {
	const token = Auth.getToken(req);
	const loggedUser = Auth.user(token);
	return res.status(201).json({ user: loggedUser});
};

module.exports = {
    register,
    login,
    getDetails
}