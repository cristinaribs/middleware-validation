const User = require('../models/User');
const {validationResult} = require('express-validator');
const {Op} = require('sequelize');

const index = async(req, res) =>{
    try{
        const user = await User.findAll();
        return res.status(200).json({user});
    } catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};


const search = async(req, res) =>{
    const {email, name} = req.body;
    try{
        const result = await User.findAll({
            where:{
                [Op.and]:{
                    email: {
                        [Op.like]: '%'+email+'%'
                    },
                    name: {
                        [Op.like]: '%'+name+'%'
                    }
                }
            }
        });
        return res.status(200).json({result});
    }catch{
        res.status(500).json({error: err});
    }
}

const create = async(req,res) => {
    try{
          validationResult(req).throw(); //validação
          const user = await User.create(req.body);
          console.log(req.body);
          return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
};

module.exports = {
    update,
    destroy,
    create,
    index,
    show,
    search
}
